import { Component, OnInit } from '@angular/core';
import {CRUDService} from "../service/crud.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  // @ts-ignore
  productForm: FormGroup;
  productId: any;
  buttonText = 'Create';

  constructor(
    private  crudService : CRUDService,
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.createProductForm();
    let productId ='';
    if(this.activatedRoute.snapshot.params['productId']) {
      productId = this.activatedRoute.snapshot.params['productId'];
      console.log('productId', productId);
      if(productId !='')
      {
        this.loadProductDetails(productId);
      }
    }
  }

  createProductForm()
  {
    this.productForm = this.formBuilder.group({
      'champ1': ['', Validators.compose([Validators.required, Validators.minLength(5)])],
      'champ2': [''],
      'champ3': ['']
    });
  }

  createProduct(values: any)
  {
    console.log(values);
    let formData = new FormData();
    formData.append('champ1', values.champ1);
    formData.append('champ2', values.champ2);
    formData.append('champ3', values.champ3);

    if(this.productId)
    {
      formData.append('id', this.productId);
      this.crudService.updateProduct(formData).subscribe(res=>{
        if(res.result==='success')
        {
          this.router.navigate(['/crud/product-list']);
        }
        else
        {

        }
      });
    }
    else
    {
      this.crudService.createProduct(formData).subscribe(res=>{
        if(res.result==='success')
        {
          this.router.navigate(['/crud/product-list']);
        }
        else
        {

        }
      });
    }
  }

  loadProductDetails(productId: any)
  {
    this.buttonText= 'Update';
    this.crudService.loadProductInfo(productId).subscribe(res=>{
      this.productForm.controls['champ1'].setValue(res.champ1);
      // this.productForm.controls.champ1.setValue(res.champ1);
      this.productId=res.id;
    })
  }
}
