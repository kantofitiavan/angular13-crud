import {Component, OnInit} from '@angular/core';
import {CRUDService} from "../service/crud.service";
import {Router} from "@angular/router";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  columnDefs=[
    {
      field: 'make',
      headerName: 'Name',
      sortable: true
    },
    { field: 'model' },
    { field: 'price'},
    {
      field: '',
      headerName: 'Actions',
      width: 250,
      cellRenderer: this.actionRender.bind(this)
    }
  ];

  rowData = [
    { make: 'Toyota', model: 'Celica', price: 35000 },
    { make: 'Ford', model: 'Mondeo', price: 32000 },
    { make: 'Porsche', model: 'Boxter', price: 72000 }
  ];

  productList: any =[];
  productSubscribe:any;

  gridOptions = {
    rowHeight: 50
  }

  constructor(
    private crudService: CRUDService,
    private router: Router
              ) {

  }

  ngOnInit(): void {
    this.getPproductList();
  }

  getPproductList()
  {
    this.productSubscribe = this.crudService.loadProducts().subscribe(res=>{
      this.productList = res;
      console.log('res', res);
    })
  }

  actionRender(params: any)
  {
    let div = document.createElement('div');
    div.innerHTML = `
      <button type="button" class="btn btn-success"> View</button>\n
      <button type="button" class="btn btn-warning">Edit</button>\n
      <button type="button" class="btn btn-danger">Delete</button>\n
    `;

    let viewButton = div.querySelector('.btn-success');
    // @ts-ignore
    viewButton.addEventListener('click', ()=>
    {
      this.viewProductsDetails(params);
    });


    let editButton = div.querySelector('.btn-warning');
    // @ts-ignore
    editButton.addEventListener('click', ()=>
    {
      this.editProductsDetails(params);
    });


    let deleteButton = div.querySelector('.btn-danger');
    // @ts-ignore
    deleteButton.addEventListener('click', ()=>
    {
      this.deleteProduct(params);
    });
    return div;
  }

  viewProductsDetails(params: any)
  {
    console.log('Param', params);
    this.router.navigate(['/crud/view-product-details/'+ params.data.make])
  }

  editProductsDetails(params: any)
  {
    console.log('Param', params);
    this.router.navigate(['/crud/update-product/'+ params.data.make])
  }

  deleteProduct(params: any)
  {
    const that = this;
    console.log('Param', params);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result: any) => {
      if (result.isConfirmed) {
        that.crudService.deleteProduct(params.data.id).subscribe(res=>{
          if(res.result === 'success')
          {
            this.getPproductList();
            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            )
          }
          else
          {
            Swal.fire(
              'Nor Deleted!',
              'Your file has not been deleted.',
              'error'
            )
          }
        });
      }
    })
  }
}
