import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {HttpResponse} from "../models/http-response";
import {Product} from "../models/product";

@Injectable({
  providedIn: 'root'
})
export class CRUDService {

  constructor(private httpClient: HttpClient) { }

  loadProducts()
  {
    const url= environment.API_EndPoint +'';
    return this.httpClient.get(url).pipe(map(data=>data));
  }

  createProduct(data: any): Observable<HttpResponse>
  {
    const url= environment.API_EndPoint +'';
    return this.httpClient.post<HttpResponse>(url, data).pipe(map(data=>data));
  }

  loadProductInfo(productId: any): Observable<Product>
  {
    const url= environment.API_EndPoint +'?id=ok';
    return this.httpClient.get<Product>(url).pipe(map(data=>data));
  }

  updateProduct(data: any): Observable<HttpResponse>
  {
    const url= environment.API_EndPoint +'update';
    return this.httpClient.post<HttpResponse>(url, data).pipe(map(data=>data));
  }

  deleteProduct(produxtId: any): Observable<HttpResponse>
  {
    const url= environment.API_EndPoint +'delete';
    return this.httpClient.get<HttpResponse>(url).pipe(map(data=>data));
  }
}
